<?php

namespace App\Controller;

use App\Agent\Log;
use App\Constants\Communication;
use App\Constants\ErrorCode;

use Hyperf\Contract\OnCloseInterface;
use Hyperf\Contract\OnMessageInterface;
use Hyperf\Contract\OnOpenInterface;
use Swoole\Http\Request;
use Swoole\Server;
use Swoole\Websocket\Frame;
use Swoole\WebSocket\Server as WebSocketServer;
use Hyperf\Di\Annotation\Inject;
use Hyperf\WebSocketServer\Context;

class WebsocketController implements OnMessageInterface, OnOpenInterface, OnCloseInterface
{

    /**
     * @Inject
     * @var WebsocketResponse
     */

    public function onMessage(WebSocketServer $server, Frame $frame): void
    {

        $server->push($frame->fd,'ok');

    }

    public function onClose(Server $server, int $fd, int $reactorId): void
    {

        var_dump('closed');
    }

    public function onOpen(WebSocketServer $server, Request $request): void
    {
        $server->push($request->fd, 'Opened');
    }
}